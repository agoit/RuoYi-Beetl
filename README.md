## 简介

ruoyi-beetl  若依项目的beetl实现   
感谢：  
http://ruoyi.vip/  
http://ibeetl.com/  
https://mp.baomidou.com/


#### 启动步骤  
````
1，使用sql脚本初始化数据库
2，启动ruoyi-admin下的App.java
3，访问http://127.0.0.1:8088/ruoyi
````


#### 功能  
>#####1，模板引擎改为beetl  
>#####2，目前封装了radio和select标签，写法简介：
````
<#select id="属性id" labels="标签1,标签2,标签3" values="值1,值2,值3" api="/api/select/user" class="cus-cls" all="请选择" required="required" dict="sys_yes_no" value="${config.configType}"/>
首先，这段代码包含了所有支持的属性，用的时候肯定不是全用的，需根据自己的要求选择

【id】一般定义属性名，会映射到标签的id和name值
【labels】和values属性一起使用，值用逗号分隔，分别对应标签option的文本和value
【api】表示选项值从接口里面获取，接口的实现参考/api/select/user
【class】自定义的标签class
【all】有这个属性的话会在选项里面加一个<option value="">请选择</option>，文本显示的是这个属性定义的值
【required】编辑的时候必须输入
【dict】表示选项值从字典中取，值为字典编码
【value】一般会在编辑页用，默认值的意思

radio的用法类似，具体实现在RadioTag和SelectTag
````
>#####3，自定义标签的实现  
````
定义一个类，继承GeneralVarTagBinding，加上TagName注解，具体参考RadioTag和SelectTag
````
>#####3，自定义函数的实现

````
定义一个类，实现Function，加上FunctionName注解，具体参考ConfigFunction和DictFunction

目前实现的函数有：
ConfigFunction
DictFunction
PermissionFunction
PrincipalFunction
RoleFunction
使用方式：
${dict('sys_oper_type','')
这个是取字典的标签，一个值的话取出来的是列表，两个值的话取出来的是单个字典，使用可以参考项目代码
````
>#####4，动态数据源使用dynamic-datasource

###有不懂的地方可以访问ruoyi和beetl官方文档，也可以加QQ群交流：652335810  