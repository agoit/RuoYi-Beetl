package com.ruoyi.framework.aspectj;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.BusinessStatus;
import com.ruoyi.common.json.JSON;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysOperLog;
import com.ruoyi.system.domain.SysUser;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

/**
 * 自动填充字段
 * 
 * @author ruoyi
 */
@Aspect
@Component
public class AutoFillAspect
{
    private static final Logger log = LoggerFactory.getLogger(AutoFillAspect.class);

    // 配置织入点
    @Pointcut("@annotation(com.ruoyi.common.annotation.AutoFill)")
    public void autoPointCut()
    {
    }

    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @Before("autoPointCut()")
    public void doBefore(JoinPoint joinPoint)
    {
        //获取目标方法的参数信息
        Object[] obj = joinPoint.getArgs();
        BaseEntity entity =  (BaseEntity) obj[0];

        String methodName = joinPoint.getSignature().getName();
        String userId = ShiroUtils.getUserId()==null?"":ShiroUtils.getUserId().toString();
        if(methodName.startsWith("add")){
            entity.setCreateBy(userId);
            entity.setCreateTime(new Date());
        }else if(methodName.startsWith("edit")){
            entity.setUpdateBy(userId);
            entity.setUpdateTime(new Date());
        }
    }


}
