package com.ruoyi.common.enums;

/**
 * 业务操作类型
 * 
 * @author ruoyi
 */
public enum ProductLogType
{
    LIFE_LOG("life_log","日常记录"),
    STATUS_CHANGE("status_change","状态变更")
    ;
    private String code;
    private String desc;

    ProductLogType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
