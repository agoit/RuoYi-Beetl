package com.ruoyi.common.enums;

public enum StatusEnums {

    SUCCESS(200,"success"),
    ERROR(999,"请求错误"),
    INPUT(403,"参数错误")

    ;

    private Integer status;
    private String desc;

    StatusEnums(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
