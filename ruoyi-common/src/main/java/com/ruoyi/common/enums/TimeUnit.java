package com.ruoyi.common.enums;

import cn.hutool.core.date.DateField;

public enum TimeUnit {

    SECOND("second", DateField.SECOND),
    MINUTES("minutes", DateField.MINUTE),
    HOUR("hour", DateField.HOUR),
    DAY("day", DateField.DAY_OF_YEAR),

    ;

    private String code;
    private DateField dateField;

    TimeUnit(String code, DateField dateField) {
        this.code = code;
        this.dateField = dateField;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DateField getDateField() {
        return dateField;
    }

    public void setDateField(DateField dateField) {
        this.dateField = dateField;
    }

    public static DateField getFieldByCode(String code){

        for (TimeUnit value : values()) {
            if(value.getCode().equals(code)){
                return value.getDateField();
            }
        }

        return null;
    }
}
