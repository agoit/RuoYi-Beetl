package com.ruoyi.common.core.bo;

import com.ruoyi.common.enums.StatusEnums;
import lombok.Data;

@Data
public class ResponseData<T> {
    private Integer status;
    private String msg;
    private T data;

    public ResponseData(Integer status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public ResponseData() {
    }



    public ResponseData(StatusEnums statusEnums, T data) {
        this.status = statusEnums.getStatus();
        this.msg = statusEnums.getDesc();
        this.data = data;
    }
    public ResponseData(StatusEnums statusEnums) {
        this.status = statusEnums.getStatus();
        this.msg = statusEnums.getDesc();
    }
    public ResponseData(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }
}
