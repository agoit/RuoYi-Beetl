package com.ruoyi.common.annotation;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.enums.OperatorType;
import com.ruoyi.common.enums.ProductLogType;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 * 
 * @author ruoyi
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ProductLog
{
    /**
     * 标题
     */
    String title() default "";

    /**
     * 功能
     */
    BusinessType businessType() default BusinessType.OTHER;

    /**
     * 日志类型
     */
    ProductLogType logType() default ProductLogType.LIFE_LOG;

    Class<?> cls();

}
