<!-- 通用CSS -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>${title}</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${staticHome}/css/bootstrap.min.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/font-awesome.min.css?v=${jsCssVersion}" rel="stylesheet"/>
    <!-- bootstrap-table 表格插件样式 -->
    <link href="${staticHome}/ajax/libs/bootstrap-table/bootstrap-table.min.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/animate.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/css/style.css?v=${jsCssVersion}" rel="stylesheet"/>
    <link href="${staticHome}/ruoyi/css/ry-ui.css?v=${jsCssVersion}" rel="stylesheet"/>
</head>