package com.ruoyi.web.core.function;

import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.web.core.anno.FunctionName;
import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 获取配置
 * @author iterking
 **/
@FunctionName("config")
@Component
public class ConfigFunction implements Function {

    @Autowired
    private ISysConfigService configService;

    @Override
    public Object call(Object[] paras, Context ctx) {
        Object o = paras[0];
        if (o != null){
            try{
                String value = configService.selectConfigByKey(o.toString());
                ctx.byteWriter.write(value.getBytes("UTF-8"));
            }catch (IOException e){
                throw new RuntimeException(e);
            }
        }
        return "";
    }
}
