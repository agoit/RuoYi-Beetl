package com.ruoyi.web.core.function;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.service.PermissionService;
import com.ruoyi.web.core.anno.FunctionName;
import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 权限验证函数
 * @author iterking
 **/
@FunctionName("role")
@Component
public class RoleFunction implements Function {

    @Autowired
    private PermissionService permissionService;

    @Override
    public Object call(Object[] paras, Context ctx) {
        if(ShiroUtils.isAdmin()){
            return true;
        }
        for (Object para : paras) {
            if(para != null){
                String permi = permissionService.hasRole(para.toString());
                if(!PermissionService.NOACCESS.equals(permi)){
                    return true;
                }
            }
        }
        return false;
    }
}
